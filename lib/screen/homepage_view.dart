import 'package:flutter/material.dart';
import 'package:pkmfilterair_apps/color_palette.dart';
import '../model/pengujian.dart';

class HomePage extends StatefulWidget{
  @override
  _HomeContent createState() => _HomeContent();
}

class _HomeContent extends State<HomePage> {
  final _formKey = GlobalKey<FormState>();


  final _tirisFilterController = TextEditingController();
  final _beratFilterController = TextEditingController();

  @override
  void dispose() {
    _tirisFilterController.dispose();
    _beratFilterController.dispose();
    super.dispose();
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text('Pengujian'),
      ),
      body: new Form(
        key: _formKey,
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: new Text(
                'Data Tiris Filtrasi (minute):',
                style: TextStyle(
                  fontSize: 17.0,
                  fontFamily: 'NotoSans',
                  fontWeight: FontWeight.w300
                ),
              ),
            ),
            new Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: new TextFormField(
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    hintText: 'Enter result value here'
                  ),
                  controller: _tirisFilterController,
                  validator: (value){
                    if(value.isEmpty){
                        return "Enter some Text";
                      }
                      return null;
                    },
              ),
            ),


            new Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: new Text(
                'Data Berat Filtrasi (gr):',
                style: TextStyle(
                  fontSize: 17.0,
                  fontFamily: 'NotoSans',
                  fontWeight: FontWeight.w300
                ),
              ),
            ),
            new Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: new TextFormField(
                  decoration: InputDecoration(
                    hintText: "Enter result value here"
                  ),
                  controller: _beratFilterController,
                  validator: (value){
                    if(value.isEmpty){
                        return "Enter some Text";
                      }
                      return null;
                    },
              ),
            ),
            new Center(
              child: new Padding(
                padding: const EdgeInsets.symmetric(vertical: 19.0),
                child: RaisedButton(
                      onPressed: (){
                        return showDialog(
                          context: context,
                          builder: (context) {
                            if(_tirisFilterController.text == ""){
                              return AlertDialog(
                              content: Text(" Silahkan cek kembali form data"),
                              );
                            }
                            else if(_beratFilterController.text ==""){
                              return AlertDialog(
                              content: Text(" Silahkan cek kembali form data"),
                              );
                            }
                            else if(_tirisFilterController.text == "" && _beratFilterController.text == ""){
                              return AlertDialog(
                              content: Text(" Silahkan cek kembali form data"),
                              );
                            }
                            else{

                                //
                              var a = Pengujian(
                                double.parse(_beratFilterController.text), 
                                double.parse(_tirisFilterController.text)
                              );

                              return AlertDialog(
                              actions: <Widget>[
                                new Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 0.0),
                                  child: RaisedButton(
                                      onPressed: (){
                                        Navigator.pop(context);
                                      },
                                      child: Text("OKE",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontFamily: "NotoSans"
                                              
                                          )
                                      ),
                                      padding:EdgeInsets.only(left: 25,right: 25,top: 13,bottom: 13),
                                      textColor: Colors.white,
                                      color: ColorPalette.dotActiveColor,
                                  ),
                                ),
                              ],
                              content: Container(
                                child: new Wrap(
                                direction: Axis.horizontal,
                                  children: <Widget>[
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10.0),
                                      child: new Text(
                                          "Hasil Pengujian :",
                                          style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.w600
                                          ),
                                        ),
                                    ),
                                    new Container(
                                      width: MediaQuery.of(context).size.width,
                                      child:  new Text(a.getResult()),
                                    )
                                  ],
                          
                              ),
                              )
                              );
                            }
                          },
                        );
                      },
                      child: Text("Show Result",
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: "NotoSans"
                              
                          )
                      ),
                      padding:EdgeInsets.only(left: 25,right: 25,top: 13,bottom: 13),
                      textColor: Colors.white,
                      color: ColorPalette.dotActiveColor,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }


}