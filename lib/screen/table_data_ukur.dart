import 'package:flutter/material.dart';
import '../screen/homepage_view.dart';
import 'package:pkmfilterair_apps/color_palette.dart';
class TableDataUkur extends StatefulWidget{

  @override
  _ContentsTable createState() => _ContentsTable();

}
class _ContentsTable extends State<TableDataUkur>{
  @override
  Widget bodyData()
  => DataTable(
      onSelectAll: (b){},
      sortColumnIndex: 0,
      sortAscending: true,
      columns: <DataColumn>[
        DataColumn(
          label: Text('Nomor',style: TextStyle(fontSize: 14.0),),
          numeric: false,
          onSort: (i,b){
            print("$i $b");
            setState(() {
              names.sort((a,b)=> a.nomor.compareTo(
              b.nomor
            )); 
            });
          },
          tooltip: "To Display firstName of the name"
        ),
        DataColumn(
          label: Text('Bersih',style: TextStyle(fontSize: 14.0),),
          numeric: false,
          onSort: (i,b){
            print("$i $b");
            setState(() {
              names.sort((a,b)=> a.airJernih.compareTo(
              b.airJernih
            )); 
            });
          },
          tooltip: "To Display firstName of the name"
        ),
        DataColumn(
          label: Text('Sedang',style: TextStyle(fontSize: 14.0),),
          numeric: false,
          tooltip: "To Display last Name of the name"
        ),
        DataColumn(
          label: Text('Kotor',style: TextStyle(fontSize: 14.0),),
          numeric: false,
          tooltip: "To Display last Name of the name"
        ),
        
      ], 
      rows: names.map((name)=> DataRow(
        cells: [
          DataCell(
            Text(name.nomor),
          ),
          DataCell(
            Text(name.airJernih),
          ),
          DataCell(
            Text(name.airSedang)
          ),
          DataCell(
            Text(name.airKotor)
          )
        ]
      )).toList()
    );

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Table'),
      ),
      body: new Material(
      elevation:0.0,  
      child: new Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        child: new SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
                children: <Widget>[
                      new Center(
                        child: new Padding(
                          padding: EdgeInsets.symmetric(horizontal: 8.0,vertical: 10.0),
                          child: new Text(
                            'Sample Data Pengukuran',
                            style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 17.0,

                            ),  
                          ),
                        ),
                      ),
                      new SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: bodyData(),
                      ),
                      new Container(
                        width: MediaQuery.of(context).size.width,
                        child: new Column(
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                new Padding(
                                  padding: EdgeInsets.only(top: 10.0, bottom: 6.0),
                                  child: new Text("Keterangan :",
                                      style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w500
                                    ),
                                  ),
                                )
                              ],
                            ),
                            new Row(
                              children: <Widget>[
                                new Text('B : Berat'),
                                new Padding(
                                  padding: EdgeInsets.only(left: 12),
                                  child: new Text('gr : Satuan Berat dalam gr(gram)'),
                                )
                              ],
                            ),
                            new Row(
                              children: <Widget>[
                                new Text('T : Tiris'),
                                new Padding(
                                  padding: EdgeInsets.only(left: 18),
                                  child: new Text('m : Satuan waktu dalam m(menit)'),
                                )
                              ],
                            ),
                            new Row(
                              children: <Widget>[
                                new Text('Bersih      : Status air dalam keadaan Bersih'),
                              ],
                            ),
                            new Row(
                              children: <Widget>[
                                new Text('Sedang     : Status air dalam keadaan Sedang'),
                              ],
                            ),
                            new Row(
                              children: <Widget>[
                                new Text('Kotor         : Status air dalam keadaan Kotor'),
                              ],
                            )
                          ],
                        ),
                      )
                      ,
                      new Center(
                          child: new Padding(
                            padding: const EdgeInsets.symmetric(vertical: 29.0),
                            child: RaisedButton(
                                onPressed: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
                                },
                                child: Text("Berikutnya",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: "NotoSans"
                                        
                                    )
                                ),
                                padding:EdgeInsets.only(left: 25,right: 25,top: 13,bottom: 13),
                                textColor: Colors.white,
                                color: ColorPalette.dotActiveColor,
                            ),
                                ),
                        )
                ],
              ),
        )
      )    
      )    
    );
  }
} 

class Name {
  String airJernih;
  String airSedang;
  String airKotor;
  String nomor;
  Name({this.nomor,this.airJernih, this.airSedang, this.airKotor});
}

var names = <Name>[
  Name(nomor: "1",airJernih: "B = 10,0gr T = 2m", airSedang: "B = 12gr T = 3,05m", airKotor: "B = 13gr T = 5.11m"),
  Name(nomor: "2",airJernih: "B = 10,2gr T = 2,02m", airSedang: "B = 12,2gr T = 3,07m", airKotor: "B = 12,9gr T = 5.10m"),
  Name(nomor: "3",airJernih: "B = 10,3gr T = 2,05m", airSedang: "B = 11,9gr T = 3,02m", airKotor: "B = 13,3gr T = 5.20m"),
  Name(nomor: "4",airJernih: "B = 10,5gr T = 2,1m", airSedang: "B = 12,3gr T = 3,1m", airKotor: "B = 13,1gr T = 5.15m"),
  Name(nomor: "5",airJernih: "B = 10,1gr T = 2.3m", airSedang: "B = 12,4gr T = 3,2m", airKotor: "B = 13,5gr T = 5.30m"),
];