import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../model/intro.dart';
import '../screen/table_data_ukur.dart';
import '../color_palette.dart';


class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  final List<Intro> introList = [
    Intro(
      image: "assets/images/sdd.png",
      title: "Perkenalan", 
      description: "Selamat datang di aplikasi SIDEKA!"+
                    "\nSIDEKA (Sistem Deteksi Kualitas Air) adalah aplikasi yang dibangun"+
                    " oleh mahasiswa dan mahasiswi Teknik Informatika Universitas Mataram."+
                    "\nAplikasi ini dibangun agar memudahkan masyarakat khususnya Lombok"+
                    " mengetahui kualitas air di daerah mereka. Dengan memanfaatkan teknologi"+
                    " Smartphone yang sudah dilengkapi dengan logic didalamnya maka anda sudah bisa mendeteksi"
                    " Kualitas air di daerah anda."
    ),
    Intro(
      image: "assets/images/ccc.png",
      title: "Apa sih Tujuan SIDEKA?",
      description: "SIDEKA bertujuan untuk membantu masyarakat yang terkena dampak bencana"+
                    " sehingga mempengaruhi kualitas air di daerah mereka. \nMengapa Air?"+
                    " Karena air adalah zat yang paling mudah terkena pencemaran"+
                    "\nUntuk itu SIDEKA dibangun untuk tujuan tersebut sehingga masyarakat"+
                    " terhindar dari penyakit yang bersumber dari pasokan air yang tidak sehat."
    ),
    Intro(
      image: "assets/images/sdsdd.png",
      title: "Bagaimana SIDEKA mendeteksi?",
      description: "SIDEKA dirancang dengan memanfaatkan logika fuzzy tsukamoto."
                    "\nKualitas air dideteksi berdasarkan berat filter air yang diperoleh"+
                    " seperti ringan, sedang, dan berat. Dari berat filter tersebut nantinya didapatkan"+
                    " kualitas air sebagaimana nilai berat yang diinputkan pengguna"
    ),
  ]; 


  @override
  Widget build(BuildContext context) {
      Widget titleSection = Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height/6,
        padding: EdgeInsets.only(bottom: 0),
        child: new Align(
          alignment: Alignment.bottomCenter,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new Text(
                'SIDEKA',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'NotoSans',
                  fontWeight: FontWeight.bold,
                  letterSpacing: 0.3,
                  color: ColorPalette.dotActiveColor,
                  fontSize:25.5,
                )
              ),
              new Text(
                'Sistem Deteksi Kualitas Air',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w300,
                  letterSpacing: 0.3,
                  color: Colors.grey[600],
                  fontSize:18.5,
                )
              ),

            ],
          )
        )
      );

      Widget navButton = Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height/5.8,       
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
             RaisedButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => TableDataUkur()));
                },
                child: Text("Get started",
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: "NotoSans"
                        
                    )
                ),
                padding:EdgeInsets.only(left: 25,right: 25,top: 13,bottom: 13),
                textColor: Colors.white,
                color: ColorPalette.dotActiveColor,
            ),
          ],
        ),
      );

      Widget onboardScreen = Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height/1.6,
          child: Swiper.children(
                index: 0,
                autoplay: true,
                loop: false,
                pagination: SwiperPagination(
                builder: DotSwiperPaginationBuilder(
                    color: ColorPalette.dotColor,
                    activeColor: ColorPalette.dotActiveColor,
                    size: 10.0,
                    activeSize: 10.0,
                 ),
                ),
                control: SwiperControl(
                  iconNext: null,
                  iconPrevious: null
                ),

                children: _buildPage(context)
                ),
      );
    return Scaffold(
        backgroundColor: Colors.white,
        body: new Wrap(
            direction: Axis.vertical,
            children: <Widget>[
                titleSection,
                onboardScreen,
                navButton
            ],
        ),
    );
  }

  List<Widget> _buildPage(BuildContext context) {

    List<Widget> widgets = [];
    for(int i=0; i<introList.length; i++) {
      Intro intro = introList[i];
      widgets.add(
        Container(
          child: ListView(
            children: <Widget>[
              Image.asset(
                intro.image,
                height: MediaQuery.of(context).size.height/5.0,
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 10,
                ),
              ),
              Center(
                child: Text(
                  intro.title,
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 19.0,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto'
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 13.0,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.height/20.0,
                ),
                child: Text(
                  intro.description,
                  style: TextStyle(
                    color: ColorPalette.descriptionColor,
                    letterSpacing: 0.5,
                    fontSize: 15.0,
                    fontFamily: 'Roboto'
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          ),
        ),
      );
    }
    return widgets;
  }


}