class Pengujian{
  final double parameterUjiBerat1 = 11.2;
  final double parameterUjiBerat2 = 12.65;
  final double paremeterUjiTiris1 = 2.75;
  final double parameterUjiTiris2 = 4.25;

  double dataBerat;
  double dataTiris;
  Pengujian(this.dataBerat,this.dataTiris);
  
  double getBerat1(){
    return this.parameterUjiBerat1;
  }
  double getBerat2(){
    return this.parameterUjiBerat2;
  }
  double getTiris1(){
    return this.paremeterUjiTiris1;
  }
  double getTiris2(){
    return this.parameterUjiTiris2;
  }

  String getResult(){
    String result;

    if(this.dataBerat <= getBerat1() && this.dataTiris <= getTiris1()){
         result = "Status Air adalah Bersih!\n"+
                  "Layak dikonsumsi langsung atau dimasak"+
                  " sebelum dikonsumsi serta untuk keperluan dapur, mandi dan lain-lain";
    }
    else if(this.dataBerat <= getBerat1() && this.dataTiris >= getTiris1()){
         result = "Status Air adalah Bersih!\n"+
                  "Layak dikonsumsi langsung atau dimasak"+
                  " sebelum dikonsumsi serta untuk keperluan dapur, mandi dan lain-lain";
    }
    else if(
      this.dataBerat >= getBerat1() &&
       this.dataTiris >= getTiris1() 
    ){
      if(this.dataBerat <= getBerat2() &&
       this.dataTiris <= getTiris2()
      ){
        result = "Status Air adalah Sedang!\n"+
                  "Tidak layak dikonsumsi, namun dapat"+
                  " digunakan untuk perikanan atau peternakan.";
      }
      else if(this.dataBerat <= getBerat2() &&
       this.dataTiris >= getTiris2()
      ){
        result = "Status Air adalah Sedang!\n"+
                  "Tidak layak dikonsumsi, namun dapat"+
                  " digunakan untuk perikanan atau peternakan.";
      }
      else{
        result = "Status Air adalah Kotor!\n"+
                  "Tidak layak dikonsumsi, namun layak"+
                  " untuk keperluan industri maupun pembangkit listrik.";
      }
      
    }
    else if(
      this.dataBerat >= getBerat2() &&
       this.dataTiris >= getTiris2() 
    ){
      result = "Status Air adalah Kotor\n"+
                  "Tidak layak dikonsumsi, namun layak"+
                  " untuk keperluan industri maupun pembangkit listrik.";
    }else{
      result = "Ups...Mohon maaf\nSilahkan cek kembali data inputan anda!";
    }
    
    return result;
  }



}