import 'package:flutter/material.dart';

class ColorPalette{
  static const dotColor = Color(0xffe8e8e8);
  static const dotActiveColor = Color(0xff2196f3);
  static const wrapperContentColor = Color(0xfff5f5f5); 
  static const descriptionColor = Color(0xff707070);
}